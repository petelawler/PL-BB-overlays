/*
 * Copyright (C) 2016 Peter Lawler <relwalretep@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/dts-v1/;
/plugin/;

#include <dt-bindings/board/am335x-bbw-bbb-base.h>
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/pinctrl/am33xx.h>

/ {
	compatible = "ti,beaglebone", "ti,beaglebone-black", "ti,beaglebone-green";

	/* identification */
	part-number = "PL-MEGABOARD1";
	version = "00A0";

	/* state the resources this cape uses */
	exclusive-use =
		/* the pin header uses */
		"P9.11",	/* gpio0_30 */ /* Open Sprinkler Shift Register DATA */
		"P9.12",	/* gpio1_28 */ /* Open Sprinkler Shift Register LATCH */
		"P9.13",	/* gpio0_31 */ /* Open Sprinkler Shift Register CLOCK */
		"P9.14",	/* gpio1_18 */ /* Open Sprinkler Shift Register NOE */
		"P9.15",	/* gpio1_16 */ /* Open Sprinkler Rain Sensor	*/
		"P9.19",	/* gpio0_13 */ /* Open Sprinkler I2C SCL */
		"P9.20",	/* gpio0_12 */ /* Open Sprinkler I2C SDA */
		"P9.23",	/* gpio1_17 */ /* 1 Wire */
		"P9.25",	/* gpio3_21 */ /* SHT SERIAL CLOCK */
		"P9.26",	/* gpio0_14 */ /* SHT SERIAL DATA */
		"P9.27",	/* gpio3_19 */ /* PIEZO ALARM*/
		"P8.7",		/* gpio2_2 */  /* RELAY 1 */
		"P8.8",		/* gpio2_3 */  /* RELAY 2 */
		"P8.9",		/* gpio2_5 */  /* RELAY 3 */
		"P8.10",	/* gpio2_24 */ /* RELAY 4 */
		/* "P8.11",	/* gpio2_24 */ /* HUMIDTY 1 */
		/* the hardware ip uses */
		"gpio0_30",
		"gpio1_28",
		"gpio0_31",
		"gpio1_18",
		"gpio1_16",
		"gpio0_13",
		"gpio0_12",
		"gpio1_17",
		"gpio3_21",
		"gpio3_19",
		"gpio0_14",
		"gpio2_2",
		"gpio2_3",
		"gpio2_5",
		"gpio2_24";

	fragment@0 {
		target = <&am33xx_pinmux>;
		__overlay__ {
			pl_gpio_opensprinkler_pins: pinmux_bb_gpio_opensprinkler_pins {
				pinctrl-single,pins = <
					BONE_P9_11 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P9_12 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P9_13 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P9_14 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P9_15 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P9_20 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
				>;
			};
			pl_gpio_1w_pins: pinmux_bb_gpio_1w_pins {
				pinctrl-single,pins = <
					BONE_P9_23 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
				>;
			};			
			pl_gpio_sht_pins: pinmux_bb_gpio_sht_pins {
				pinctrl-single,pins = <
					BONE_P9_25 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P9_26 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
				>;
			};
			pl_gpio_piezo_pins: pinmux_bb_gpio_1w_pins {
				pinctrl-single,pins = <
					BONE_P9_27 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
				>;
			};	
			pl_gpio_relay_pins: pinmux_bb_gpio_relay_pins {
				pinctrl-single,pins = <
					BONE_P8_08 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P8_10 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P8_12 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
					BONE_P8_14 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
				>;
			};
		};
	};

	fragment@1 {
		target-path="/";
		__overlay__ {

			leds {
				pinctrl-names = "default";
				pinctrl-0 = <&pl_gpio_opensprinkler_pins>;
				pinctrl-1 = <&pl_gpio_1w_pins>;
				pinctrl-2 = <&pl_gpio_sht_pins>;
				pinctrl-3 = <&pl_gpio_piezo_pins>;
				pinctrl-4 = <&pl_gpio_relay_pins>;

/* *** DINNER BREAK ****    */

				compatible = "gpio-leds";

				jp@1 {
					label = "relay-jp1";
					gpios = <&gpio2 3 GPIO_ACTIVE_HIGH>;
					default-state = "keep";
				};

				jp@2 {
					label = "relay-jp2";
					gpios = <&gpio2 4 GPIO_ACTIVE_HIGH>;
					default-state = "keep";
				};

				jp@3 {
					label = "relay-jp3";
					gpios = <&gpio1 12 GPIO_ACTIVE_HIGH>;
					default-state = "keep";
				};

				jp@4 {
					label = "relay-jp4";
					gpios = <&gpio0 26 GPIO_ACTIVE_HIGH>;
					default-state = "keep";
				};
			};
		};
	};
};
