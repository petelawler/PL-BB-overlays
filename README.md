# PL-BB-overlays
Beaglebone Black device tree overlays

Working overlays for:
* 1 Port relay on P8.27
* 4 Port relay on P8.8,P8.10,P8.12,P8.14
* UART4
* Dallas 1-Wire on P9.27
* PIEZO Alarm on P9.30

Notes
* Freetronics OLED overlay is very much a work in progress.
* OpenSprinkler overlay is a work in progress.
* Megaboard overlay is a work in progress (will eventually combine all of above).

Use https://github.com/BeagleBoard/bb.org-overlays and copy the needed file into src/arm,
then cd src/arm on that tree and then install.sh (fist install the patched.dtc if needs be)

