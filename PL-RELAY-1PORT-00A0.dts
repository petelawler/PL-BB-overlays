/*
 * Copyright (C) 2015 Robert Nelson <robertcnelson@gmail.com>
 * Copyright (C) 2016 Peter Lawler <relwalretep@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/dts-v1/;
/plugin/;

#include <dt-bindings/board/am335x-bbw-bbb-base.h>
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/pinctrl/am33xx.h>

/ {
	compatible = "ti,beaglebone", "ti,beaglebone-black", "ti,beaglebone-green";

	/* identification */
	part-number = "PL-RELAY-1PORT";
	version = "00A0";

	/* state the resources this cape uses */
	exclusive-use =
		/* the pin header uses */
		"P8.7",		/* gpio2_2 */
		/* the hardware ip uses */
		"gpio2_2";

	fragment@0 {
		target = <&am33xx_pinmux>;
		__overlay__ {

			pl_gpio_relay_pins: pinmux_pl_gpio_relay_pins {
				pinctrl-single,pins = <
					BONE_P8_07 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)
				>;
			};

		};
	};

	fragment@1 {
		target-path="/";
		__overlay__ {

			leds {
				pinctrl-names = "default";
				pinctrl-0 = <&pl_gpio_relay_pins>;

				compatible = "gpio-leds";

				jp@1 {
					label = "relay-jp1";
					gpios = <&gpio2 2 GPIO_ACTIVE_LOW>;
					default-state = "keep";
				};

			};
		};
	};
};
